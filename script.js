bg=$('#background');
var loopi=1;
var loopig=0;
var ls=50;
var chw=0;
function ad(tag, dat) {$('body').data(tag, dat);}//Store data
function rd(tag) {return $('body').data(tag);}//Retrieve Data
function rand(min,max){return Math.floor(Math.random() * (max - min + 1)) + min;}//random
function pi(n){return parseInt(n,10);}
function mr(n){return Math.round(n);}
function pause(){if(loopig==1) {loopig=0;clearInterval(loopi);}}
function resume(){if(loopig==0){loopi=setInterval(function(){loop();},ls);loopig=1;}}
function createNewBackground(xx,yy){
    sdp=0;
    ad('maxWidth',xx);
    ad('maxHeight',yy);
    window.xx=xx;
    window.yy=yy;
    var a;
    var b=[];
    for(y=0;y<yy;y++){
        for(x=0;x<xx;x++){
            ab=[0,0,0,0,0,0,0,0];
            tl=(y-1>-1&&x-1>-1)?b[(y-1)*xx+x-1]:ab;
            tm=(y-1>-1)?b[(y-1)*xx+x]:ab;
            tr=(y-1>-1&&x+1<xx)?b[(y-1)*xx+x+1]:ab;
            ml=(x-1>-1)?b[y*xx+x-1]:ab;
            var d=mv((tl[6]+tm[6]+tr[6]+ml[6])/4,10);
            var e=mv((tl[7]+tm[7]+tr[7]+ml[7])/4,10);
            var dsv=Math.floor(128+((d/-50)*128));
            var dm="rgb("+dsv+","+dsv+","+dsv+");";
            var em="rgb("+Math.floor(128+e*-2.56)+","+ Math.floor(128+e*2.56)+","+0+");";
            var g='c'+x+'x'+y;
            a+="<div class='cell' style='left:"+x*20+"px; top:"+y*20+"px; border-top-color:"+dm+"; border-bottom-color:"+dm+"; border-left-color:"+em+"; border-right-color:"+em+";' id='"+g+"'></div>";
            var c=(y*yy)+x;
            b[c]=[];
            b[c][0]=0;
            b[c][1]=0;
            b[c][2]=0;
            b[c][3]=0;
            b[c][4]=0;
            b[c][5]=0;
            b[c][6]=d;
            b[c][7]=e;
        }
    }
    b[0][0]=1;
    b[xx-1][0]=2;
    b[xx*(yy-1)][0]=3;
    b[(xx*yy)-1][0]=4;
    ad('ba',b);
    bg.html(a);
    for(y=0;y<yy;y++){
        for(x=0;x<xx;x++){
            $('#c'+x+'x'+y).bind('mousedown',function(){
                var a=$(this).attr('id').split('c')[1].split('x');
                startTooltip(a[0],a[1]);
            });
        }
    }
    $('#ns').css('top',(xx*20)+40);
    resume();
}
function updateSpace(x,y,a){
    var b=$('#c'+x+'x'+y);
    var c;
    switch(a[0]){
        case 0:
            c='FFFFFF';
            break;
        case 1:
            c='FFCC00';
            break;
        case 2:
            c='00CC00';
            break;
        case 3:
            c='0000FF';
            break;
        case 4:
            c='CC00CC';
            break;
    }
            
    /*if(a[0]==0) c='FFFFFF';
    if(a[0]==1) c='FFCC00';
    if(a[0]==2) c='00CC00';
    if(a[0]==3) c='0000FF';
    if(a[0]==4) c='CC00CC';*/
    b.css("background-color",'#'+c);
}
function cCS(arr,arr2,arr3){//1=from 2=to 3=array to modify
    if(arr[0]!=0){
        arr3[arr[0]-1][0]+=Math.floor(((((Math.abs(arr2[6]-arr[4])-50)/-4)+((Math.abs(arr2[7]-arr[5])-50)/-4))+50)/8);
        arr3[arr[0]-1][1]+=arr[1];
        arr3[arr[0]-1][2]+=arr[2];
        arr3[arr[0]-1][3]+=arr[3];
        arr3[arr[0]-1][4]+=arr[4];
        arr3[arr[0]-1][5]+=arr[5];
        arr3[arr[0]-1][6]+=1;
        return arr3;
    }else{
        return arr3;
    }
}
function startTooltip(x,y){
    var tt=$('#tooltip');
    tt.show();
    var c=rd('ba')[pi(y)*rd('maxWidth')+pi(x)];
    //console.log(pi(y)+' '+rd('maxWidth')+' '+pi(x));
    tt.html("Player: "+c[0]+" Combat: "+c[3]+"<br />cRes: "+c[4]+" occRes: "+c[5]);
    chw=[x,y];
}
function mv(n,f){
    var a=rand(-f,f);
    var d=rand(1,5)<5?n+a:rand(1,5)<5?n+(a*2):n+(a*3);
    d=d>=-50?d:-50;
    d=d<=50?d:50;
    return d;
}
function occupy(defs,pl,arr,mmc){
    var a;
    if(defs[0]==0){
        a=1;
    }else{
        var tcs=defs[3]+100+arr[3];
        if(rand(1,tcs)<defs[3]+50){
            return;
        }else{
            a=1;
        }
    }
    if(a==1){
        var f=rd('ba');
        f[mmc][0]=pl;
        f[mmc][1]=mr(arr[1]);
        f[mmc][2]=mr(arr[2]);
        f[mmc][3]=mr(arr[3]);
        f[mmc][4]=mr(arr[4]);
        f[mmc][5]=mr(arr[5]);
        var e=rd('maxHeight');
        //updateSpace(Math.floor(mmc/e),mmc%e);
    }
}
var sdp=0;
function loop(){
    var a=rd('ba');
    var mh=rd('maxHeight');
    var mw=rd('maxWidth');
    var ab=[0,0,0,0,0,0,0,0];
    var tl;var tm;var tr;var ml;var mm;var mr;var bl;var bm;var br;
    var p1n=0, p2n=0, p3n=0, p4n=0;
    var p1e=0;
    var p2e=0;
    var p3e=0;
    var p4e=0;
    var da;
    var dav;
    for(y=0;y<mh;y++){
        for(x=0;x<mw;x++){
            var mmc=y*mw+x;
            tl=(y-1>-1&&x-1>-1)?a[(y-1)*mw+x-1]:ab;
            tm=(y-1>-1)?a[(y-1)*mw+x]:ab;
            tr=(y-1>-1&&x+1<mw)?a[(y-1)*mw+x+1]:ab;
            ml=(x-1>-1)?a[y*mw+x-1]:ab;
            mm=a[y*mw+x];
            mr=(x+1<mw)?a[y*mw+x+1]:ab;
            bl=(y+1<mh&&x-1>-1)?a[(y+1)*mw+x-1]:ab;
            bm=(y+1<mh)?a[(y+1)*mw+x]:ab;
            br=(y+1<mh&&x+1<mw)?a[(y+1)*mw+x+1]:ab;
            if(tl[0]!=0 ||tm[0]!=0 ||tr[0]!=0 ||ml[0]!=0 ||mr[0]!=0 ||bl[0]!=0 ||bm[0]!=0 ||br[0]!=0){
                var b=[[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0]];
                //1 is mutation rate, 2 is rep rate, 3 is Combat strength, 4 is temperature, 5 is occ survival, 6 is number, 0 is rep chance
 b=cCS(tl,mm,b);b=cCS(tm,mm,b);b=cCS(tr,mm,b);b=cCS(ml,mm,b);b=cCS(mr,mm,b);b=cCS(bl,mm,b);b=cCS(bm,mm,b);b=cCS(br,mm,b);
                for (var i=0;i<b.length;i++) {
                    b[i][1]=0;
                    b[i][2]=mv(b[i][2]/b[i][6],4);
                    b[i][3]=mv(b[i][3]/b[i][6],4);
                    b[i][4]=mv(b[i][4]/b[i][6],4);
                    b[i][5]=mv(b[i][5]/b[i][6],4);
                }
                var is1=mm[0]==1?1:0;var is2=mm[0]==2?1:0;var is3=mm[0]==3?1:0;var is4=mm[0]==4?1:0;
                if(b[0][0]>=rand(1,100)&&is1==0) occupy(mm,1,b[0],mmc);
                if(b[1][0]>=rand(1,100)&&is2==0) occupy(mm,2,b[1],mmc);
                if(b[2][0]>=rand(1,100)&&is3==0) occupy(mm,3,b[2],mmc);
                if(b[3][0]>=rand(1,100)&&is4==0) occupy(mm,4,b[3],mmc);
                updateSpace(x,y,mm);
            }
            dav=((-mm[3]+50)/1.72)+6;
            da=rand(1,dav);
            if(sdp==20){
                if(da==1){
                    mm[0]=0;
                    updateSpace(x,y,mm);
                }
            }
            switch(mm[0]){
                case 1:
                    p1n++;
                    p1e=1;
                    break;
                case 2:
                    p2n++;
                    p2e=1;
                    break;
                case 3:
                    p3n++;
                    p3e=1;
                    break;
                case 4:
                    p4n++;
                    p4e=1;
                    break;
            }
        }
    }
    if(p1e+p2e+p3e+p4e==1&&2==1){
        pause();
        if(p1e==1){
            bg.html("<h1>PLAYER 1 WINS!</h1>");
        }
        if(p2e==1){
            bg.html("<h1>PLAYER 2 WINS!</h1>");
        }
        if(p3e==1){
            bg.html("<h1>PLAYER 3 WINS!</h1>");
        }
        if(p4e==1){
            bg.html("<h1>PLAYER 4 WINS!</h1>");
        }
    }
    $('#ns').html('Player 1 '+p1n+'<br />Player 2 '+p2n+'<br />Player 3 '+p3n+'<br />Player 4 '+p4n);
    if(sdp<20){
        sdp++;
    }
}
createNewBackground(25,25);
loopig=1;
$(document).keydown(function(event){
    if(event.which==80){
        var a=1;
        if(loopig==1&&a==1) {pause();a=0;}
        if(loopig==0&&a==1) {resume();a=0;}
    }
    if(event.which==82){
        $('#restartTT').show();
        pause();
    }
    if(event.which==76){
        $('#loopTT').show();
        pause();
    }
});
$(document).keyup(function(event){
    if(event.which==82){
        $('#restartTT').hide();
        var a=pi($('#resize').val());
        createNewBackground(a,a);
        resume();
    }
    if(event.which==76){
        ls=$('#msd').val();
        resume();
        $('#loopTT').hide();
    }
});
$('#msd').on("change mousemove", function() {
    $('#mdsv').html($(this).val());
});
$('body').mouseup(function(){
    if(chw!=0){
        
        chw=0;
    }
    $('#tooltip').hide();
});
